from pyspark.sql import SparkSession

PATH = 'web-BerkStan.txt.gz'
ITERATIONS = 2

spark = SparkSession.builder\
    .master('local')\
    .appName('pagerank')\
    .getOrCreate()
sc = spark.sparkContext

# read the data and filter out the first comments
rdd = sc.textFile(PATH).filter(lambda x: not x.startswith('#'))


# split row into 2 urls
def split(row):
    parts = row.split('\t')
    return parts[0], parts[1]

# (url, [to_urls])
links = rdd.map(lambda x: split(x)).distinct().groupByKey().cache()
# (url, rank)
ranks = links.mapValues(lambda x: 1.0)


# compute new partial rank for each linked to url
def divide(urls_rank):
    rank = urls_rank[1]
    number_urls = len(urls_rank[0])
    res = []
    for url in urls_rank[0]:
        res.append((url, rank / number_urls))
    return res

# actual algorithm iterations
for i in range(ITERATIONS):
    # join to get (url, ([to_urls], rank)), then flatMap the values
    contributions = links\
        .join(ranks)\
        .values()\
        .flatMap(lambda urls_rank: divide(urls_rank))

    # combine the partial ranks and apply formula
    ranks = contributions\
        .reduceByKey(lambda x, y: x + y)\
        .mapValues(lambda x: x * 0.85 + 0.15)

# sort by most likely to be clicked
output = ranks.sortBy(lambda x: x[1], ascending=False)

# (url, score)
print(output.take(10))
